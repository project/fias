<?php

/**
 * @file
 * Contains \Drupal\fias\HouseintListBuilder.
 */

namespace Drupal\fias;

use Drupal\Component\Utility\SafeMarkup;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

/**
 * Defines a class to build a listing of fias house interval entities.
 *
 * @see \Drupal\fias\Entity\Houseint
 */
class HouseintListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = array(
      'guid' => $this->t('GUID'),
      'intstart' => 'Начало интервала',
      'intend' => 'Окончание интервала',
      'intstatus' => 'Статус интервала',
    );
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['guid'] = SafeMarkup::checkPlain($entity->get('intguid')->value);
    $row['intstart'] = SafeMarkup::checkPlain($entity->get('intstart')->value);
    $row['intend'] = SafeMarkup::checkPlain($entity->get('intend')->value);
    $row['intstatus'] = SafeMarkup::checkPlain($entity->get('intstatus')->value);
    return $row + parent::buildRow($entity);
  }

}
