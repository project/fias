<?php

/**
 * @file
 * Contains \Drupal\fias\AddressObjectStorageSchema.
 */

namespace Drupal\fias;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorageSchema;

class AddressObjectStorageSchema extends SqlContentEntityStorageSchema {

  /**
   * {@inheritdoc}
   */
  protected function getEntitySchema(ContentEntityTypeInterface $entity_type, $reset = FALSE) {
    $schema = parent::getEntitySchema($entity_type, $reset);

    $schema['fias_addrobj']['indexes'] += [
      'aoguid' => ['aoguid'],
      'parentguid' => ['parentguid'],
      'currstatus' => ['currstatus'],
      'aolevel' => ['aolevel'],
    ];

    return $schema;
  }

}
