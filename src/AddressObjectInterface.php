<?php

/**
 * @file
 * Contains \Drupal\fias\AddressObjectInterface.
 */

namespace Drupal\fias;

use Drupal\Core\Entity\ContentEntityInterface;

interface AddressObjectInterface extends ContentEntityInterface {

  /**
   * Return TRUE if address object is a region (top-level address object with
   * aolevel = 1).
   *
   * @return bool
   *   TRUE if entity is a region, FALSE otherwise.
   */
  public function isRegion();

  /**
   * Return full address string (with parents).
   *
   * @return string
   *   Full address string.
   */
  public function getFullAddress();

}
