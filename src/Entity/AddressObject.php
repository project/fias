<?php

/**
 * @file
 * Contains \Drupal\fias\Entity\AddressObject.
 */

namespace Drupal\fias\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\fias\AddressObjectInterface;
use Drupal\fias\AddressObjectStorageInterface;

/**
 * Defines an AddressObject entity class.
 *
 * @ContentEntityType(
 *   id = "fias_address_object",
 *   label = @Translation("FIAS Address Object"),
 *   handlers = {
 *     "storage" = "Drupal\fias\AddressObjectStorage",
 *     "storage_schema" = "Drupal\fias\AddressObjectStorageSchema",
 *     "list_builder" = "Drupal\fias\AddressObjectListBuilder",
 *     "views_data" = "Drupal\fias\AddressObjectViewsData",
 *     "form" = {
 *       "default" = "Drupal\fias\AddressObjectForm",
 *       "add" = "Drupal\fias\AddressObjectForm",
 *       "edit" = "Drupal\fias\AddressObjectForm",
 *       "delete" = "Drupal\fias\Form\AddressObjectDeleteForm"
 *     }
 *   },
 *   base_table = "fias_addrobj",
 *   translatable = FALSE,
 *   admin_permission = "administer fias address objects",
 *   entity_keys = {
 *     "id" = "aoid",
 *     "label" = "offname",
 *   },
 *   field_ui_base_route = "entity.fias_address_object.collection",
 *   links = {
 *     "canonical" = "admin/config/content/fias/address_object/{fias_address_object}",
 *     "delete-form" = "admin/config/content/fias/address_object/{fias_address_object}/delete",
 *     "edit-form" = "admin/config/content/fias/address_object/{fias_address_object}/edit"
 *   }
 * )
 */
class AddressObject extends FiasEntityBase implements AddressObjectInterface {

  /**
   * {@inheritdoc}
   */
  public function getGuidFieldName() {
    return 'aoguid';
  }

  /**
   * {@inheritdoc}
   */
  public function isRegion() {
    return $this->get('aolevel')->value == AddressObjectStorageInterface::LEVEL_REGION;
  }

  /**
   * {@inheritdoc}
   */
  public function getFullAddress() {
    /** @var \Drupal\fias\AddressObjectStorageInterface $storage */
    $storage = \Drupal::entityManager()->getStorage('fias_address_object');
    $parents = $storage->getAllParents($this->guid());
    $parents = array_reverse($parents);

    foreach ($parents as &$parent) {
      $parent = $storage->loadByGuid($parent);
      $parent = $parent->label();
    }

    $parents[] = $this->label();

    return implode(', ', $parents);
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    $shortname = $this->get('shortname')->value;
    $offname = $this->get('offname')->value;
    switch ($shortname) {
      case 'Респ':
        $label = 'Республика ' . $offname;
        break;
      case 'г':
        $label = 'город ' . $offname;
        break;
      case 'пгт':
        $label = 'поселок городского типа ' . $offname;
        break;
      case 'п':
        $label = 'поселок ' . $offname;
        break;
      case 'с':
        $label = 'село ' . $offname;
        break;
      case 'рп':
        $label = 'рабочий поселок ' . $offname;
        break;
      case 'х':
        $label = 'хутор ' . $offname;
        break;
      case 'аул':
        $label = 'аул ' . $offname;
        break;
      default:
        $label = $offname . ' ' . $shortname;
    }
    return $label;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['aoguid'] = BaseFieldDefinition::create('string')
      ->setLabel('Глобальный уникальный идентификатор адресного объекта')
      ->setSetting('max_length', 36);

    $fields['formalname'] = BaseFieldDefinition::create('string')
      ->setLabel('Формализованное наименование')
      ->setSetting('max_length', 120);

    $fields['regioncode'] = BaseFieldDefinition::create('string')
      ->setLabel('Код региона')
      ->setSetting('max_length', 2);

    $fields['autocode'] = BaseFieldDefinition::create('string')
      ->setLabel('Код автономии')
      ->setSetting('max_length', 1);

    $fields['areacode'] = BaseFieldDefinition::create('string')
      ->setLabel('Код района')
      ->setSetting('max_length', 3);

    $fields['citycode'] = BaseFieldDefinition::create('string')
      ->setLabel('Код города')
      ->setSetting('max_length', 3);

    $fields['ctarcode'] = BaseFieldDefinition::create('string')
      ->setLabel('Код внутригородского района')
      ->setSetting('max_length', 3);

    $fields['placecode'] = BaseFieldDefinition::create('string')
      ->setLabel('Код населенного пункта')
      ->setSetting('max_length', 3);

    $fields['streetcode'] = BaseFieldDefinition::create('string')
      ->setLabel('Код улицы')
      ->setSetting('max_length', 4);

    $fields['extrcode'] = BaseFieldDefinition::create('string')
      ->setLabel('Код дополнительного адресообразующего элемента')
      ->setSetting('max_length', 4);

    $fields['sextcode'] = BaseFieldDefinition::create('string')
      ->setLabel('Код подчиненного дополнительного адресообразующего элемента')
      ->setSetting('max_length', 3);

    $fields['offname'] = BaseFieldDefinition::create('string')
      ->setLabel('Официальное наименование')
      ->setSetting('max_length', 120);

    $fields['shortname'] = BaseFieldDefinition::create('string')
      ->setLabel('Краткое наименование типа объекта')
      ->setSetting('max_length', 10);

    $fields['aolevel'] = BaseFieldDefinition::create('integer')
      ->setLabel('Уровень адресного объекта');

    $fields['parentguid'] = BaseFieldDefinition::create('string')
      ->setLabel('Идентификатор объекта родительского объекта')
      ->setSetting('max_length', 36);

    $fields['aoid'] = BaseFieldDefinition::create('string')
      ->setLabel('Уникальный идентификатор записи. Ключевое поле')
      ->setSetting('max_length', 36);

    $fields['previd'] = BaseFieldDefinition::create('string')
      ->setLabel('Идентификатор записи связывания с предыдушей исторической записью')
      ->setSetting('max_length', 36);

    $fields['nextid'] = BaseFieldDefinition::create('string')
      ->setLabel('Идентификатор записи  связывания с последующей исторической записью')
      ->setSetting('max_length', 36);

    $fields['code'] = BaseFieldDefinition::create('string')
      ->setLabel('Код адресного объекта одной строкой с признаком актуальности из КЛАДР 4.0')
      ->setSetting('max_length', 17);

    $fields['plaincode'] = BaseFieldDefinition::create('string')
      ->setLabel('Код адресного объекта из КЛАДР 4.0 одной строкой без признака актуальности (последних двух цифр)')
      ->setSetting('max_length', 15);

    $fields['actstatus'] = BaseFieldDefinition::create('integer')
      ->setLabel('Статус актуальности адресного объекта ФИАС')
      ->setDescription('Статус актуальности адресного объекта ФИАС. Актуальный адрес на текущую дату. Обычно последняя запись об адресном объекте. 0 – Не актуальный 1 - Актуальный');

    $fields['centstatus'] = BaseFieldDefinition::create('integer')
      ->setLabel('Статус центра');

    $fields['operstatus'] = BaseFieldDefinition::create('integer')
      ->setLabel('Статус действия над записью')
      ->setDescription('Статус действия над записью – причина появления записи (см. описание таблицы OperationStatus):01 – Инициация;10 – Добавление;20 – Изменение;21 – Групповое изменение;30 – Удаление;31 - Удаление вследствие удаления вышестоящего объекта;40 – Присоединение адресного объекта (слияние);41 – Переподчинение вследствие слияния вышестоящего объекта;42 - Прекращение существования вследствие присоединения к другому адресному объекту;43 - Создание нового адресного объекта в результате слияния адресных объектов;50 – Переподчинение;51 – Переподчинение вследствие переподчинения вышестоящего объекта;60 – Прекращение существования вследствие дробления;61 – Создание нового адресного объекта в результате дробления;70 – Восстановление прекратившего существование объекта');

    $fields['currstatus'] = BaseFieldDefinition::create('integer')
      ->setLabel('Статус актуальности КЛАДР 4 (последние две цифры в коде)');

    $fields['normdoc'] = BaseFieldDefinition::create('string')
      ->setLabel('Внешний ключ на нормативный документ')
      ->setSetting('max_length', 36);

    static::setDefaultFieldsSettings($fields);

    return $fields;
  }

}
