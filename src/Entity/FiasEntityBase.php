<?php

/**
 * @file
 * Contains \Drupal\fias\Entity\FiasEntityBase.
 */

namespace Drupal\fias\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Abstract class with usefull methods for fias entities.
 */
abstract class FiasEntityBase extends ContentEntityBase {

  /**
   * Loads an entity by it's GUID.
   *
   * @param string $guid
   *   The GUID of the entity to load.
   *
   * @return static
   *   The entity object or NULL if there is no entity with the given GUID.
   */
  public static function loadByGuid($guid) {
    $entity_manager = \Drupal::entityManager();
    $entity_type = $entity_manager->getEntityTypeFromClass(get_called_class());
    return $entity_manager->getStorage($entity_type)->loadByGuid($guid);
  }

  /**
   * Return the field name that store GUID of this entity.
   *
   * This function can not be static due to PHP Strict Standards.
   *
   * @return string
   *   The string with GUID field name of this entity.
   */
  abstract public function getGuidFieldName();

  /**
   * Return the GUID for this entity.
   *
   * @return string
   *   The string with GUID value for this entity.
   */
  public function guid() {
    return $this->get($this->getGuidFieldName())->value;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = [];

    $fields['postalcode'] = BaseFieldDefinition::create('string')
      ->setLabel('Почтовый индекс')
      ->setSetting('max_length', 6);

    $fields['ifnsfl'] = BaseFieldDefinition::create('string')
      ->setLabel('Код ИФНС ФЛ')
      ->setSetting('max_length', 4);

    $fields['terrifnsfl'] = BaseFieldDefinition::create('string')
      ->setLabel('Код территориального участка ИФНС ФЛ')
      ->setSetting('max_length', 4);

    $fields['ifnsul'] = BaseFieldDefinition::create('string')
      ->setLabel('Код ИФНС ЮЛ')
      ->setSetting('max_length', 4);

    $fields['terrifnsul'] = BaseFieldDefinition::create('string')
      ->setLabel('Код территориального участка ИФНС ЮЛ')
      ->setSetting('max_length', 4);

    $fields['okato'] = BaseFieldDefinition::create('string')
      ->setLabel('ОКАТО')
      ->setSetting('max_length', 11);

    $fields['oktmo'] = BaseFieldDefinition::create('string')
      ->setLabel('ОКТМО')
      ->setSetting('max_length', 11);

    $fields['updatedate'] = BaseFieldDefinition::create('datetime')
      ->setLabel('Дата внесения (обновления) записи')
      ->setSetting('datetime_type', 'date');

    $fields['startdate'] = BaseFieldDefinition::create('datetime')
      ->setLabel('Начало действия записи')
      ->setSetting('datetime_type', 'date');

    $fields['enddate'] = BaseFieldDefinition::create('datetime')
      ->setLabel('Окончание действия записи')
      ->setSetting('datetime_type', 'date');

    return $fields;
  }

  /**
   * This function set default parameters for fias entities fields.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface[] $fields
   *   The fields array.
   * @return void
   */
  protected static function setDefaultFieldsSettings(&$fields) {
    /** @var \Drupal\Core\Field\BaseFieldDefinition $field */
    foreach ($fields as $field_name => $field) {

      // Set readonly and required flags for all fields
      $field->setReadOnly(TRUE)->setRequired(FALSE);

      switch ($field->getType()) {
        case 'string':
          $string_length = $field->getSetting('max_length');
          $field->setDisplayOptions('view', array(
            'label' => 'inline',
            'type' => 'string',
          ))
            ->setDisplayConfigurable('view', TRUE)
            ->setDisplayOptions('form', array(
              'type' => 'string_textfield',
              'settings' => array(
                'size' => $string_length,
              ),
            ))
            ->setDisplayConfigurable('form', TRUE)
            ->setPropertyConstraints('value', array('Length' => array('max' => $string_length)));
          break;
        case 'datetime':
          $field->setDisplayOptions('view', array(
            'label' => 'hidden',
            'type' => 'datetime_default',
            'weight' => 1,
          ))
          ->setDisplayConfigurable('view', TRUE)
          ->setDisplayOptions('form', array(
            'type' => 'datetime_default',
            'weight' => 1,
            'settings' => [],
          ))
          ->setDisplayConfigurable('form', TRUE);
          break;
      }
    }
  }

}
