<?php

/**
 * @file
 * Contains \Drupal\fias\Entity\Houseint.
 */

namespace Drupal\fias\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\fias\HouseintInterface;

/**
 * Defines an Houseint entity class.
 *
 * @ContentEntityType(
 *   id = "fias_houseint",
 *   label = @Translation("FIAS House Interval"),
 *   handlers = {
 *     "storage" = "Drupal\fias\HouseintStorage",
 *     "storage_schema" = "Drupal\fias\HouseintStorageSchema",
 *     "list_builder" = "Drupal\fias\HouseintListBuilder",
 *     "views_data" = "Drupal\fias\HouseintViewsData",
 *     "form" = {
 *       "default" = "Drupal\fias\HouseintForm",
 *       "add" = "Drupal\fias\HouseintForm",
 *       "edit" = "Drupal\fias\HouseintForm",
 *       "delete" = "Drupal\fias\Form\HouseintDeleteForm"
 *     }
 *   },
 *   base_table = "fias_houseint",
 *   translatable = FALSE,
 *   admin_permission = "administer fias house intervals",
 *   entity_keys = {
 *     "id" = "houseintid",
 *     "label" = "intstatus",
 *   },
 *   field_ui_base_route = "entity.fias_houseint.collection",
 *   links = {
 *     "canonical" = "admin/config/content/fias/house/{fias_houseint}",
 *     "delete-form" = "admin/config/content/fias/house/{fias_houseint}/delete",
 *     "edit-form" = "admin/config/content/fias/house/{fias_houseint}/edit"
 *   }
 * )
 */
class Houseint extends FiasEntityBase implements HouseintInterface {

  /**
   * {@inheritdoc}
   */
  public function getGuidFieldName() {
    return 'INTGUID';
  }

    /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['intstart'] = BaseFieldDefinition::create('integer')
      ->setLabel('Значение начала интервала');

    $fields['intend'] = BaseFieldDefinition::create('integer')
      ->setLabel('Значение окончания интервала');

    $fields['houseintid'] = BaseFieldDefinition::create('string')
      ->setLabel('Идентификатор записи интервала домов')
      ->setSetting('max_length', 36);

    $fields['intguid'] = BaseFieldDefinition::create('string')
      ->setLabel('Глобальный уникальный идентификатор интервала домов')
      ->setSetting('max_length', 36);

    $fields['aoguid'] = BaseFieldDefinition::create('string')
      ->setLabel('Идентификатор объекта родительского объекта (улицы, города, населенного пункта и т.п.)')
      ->setSetting('max_length', 36);

    $fields['intstatus'] = BaseFieldDefinition::create('integer')
      ->setLabel('Статус интервала (обычный, четный, нечетный)');

    $fields['normdoc'] = BaseFieldDefinition::create('string')
      ->setLabel('Внешний ключ на нормативный документ')
      ->setSetting('max_length', 36);

    $fields['counter'] = BaseFieldDefinition::create('integer')
      ->setLabel('Счетчик записей домов для КЛАДР 4');

    static::setDefaultFieldsSettings($fields);

    return $fields;
  }

}
