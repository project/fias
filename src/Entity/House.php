<?php

/**
 * @file
 * Contains \Drupal\fias\Entity\House.
 */

namespace Drupal\fias\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\fias\HouseInterface;

/**
 * Defines an House entity class.
 *
 * @ContentEntityType(
 *   id = "fias_house",
 *   label = @Translation("FIAS House"),
 *   handlers = {
 *     "storage" = "Drupal\fias\HouseStorage",
 *     "storage_schema" = "Drupal\fias\HouseStorageSchema",
 *     "list_builder" = "Drupal\fias\HouseListBuilder",
 *     "views_data" = "Drupal\fias\HouseViewsData",
 *     "form" = {
 *       "default" = "Drupal\fias\HouseForm",
 *       "add" = "Drupal\fias\HouseForm",
 *       "edit" = "Drupal\fias\HouseForm",
 *       "delete" = "Drupal\fias\Form\HouseDeleteForm"
 *     }
 *   },
 *   base_table = "fias_house",
 *   translatable = FALSE,
 *   admin_permission = "administer fias houses",
 *   entity_keys = {
 *     "id" = "houseid",
 *     "label" = "housenum",
 *   },
 *   field_ui_base_route = "entity.fias_house.collection",
 *   links = {
 *     "canonical" = "admin/config/content/fias/house/{fias_house}",
 *     "delete-form" = "admin/config/content/fias/house/{fias_house}/delete",
 *     "edit-form" = "admin/config/content/fias/house/{fias_house}/edit"
 *   }
 * )
 */
class House extends FiasEntityBase implements HouseInterface {

  /**
   * {@inheritdoc}
   */
  public function getGuidFieldName() {
    return 'houseguid';
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['housenum'] = BaseFieldDefinition::create('string')
      ->setLabel('Номер дома')
      ->setSetting('max_length', 20);

    $fields['eststatus'] = BaseFieldDefinition::create('integer')
      ->setLabel('Признак владения');

    $fields['buildnum'] = BaseFieldDefinition::create('string')
      ->setLabel('Номер корпуса')
      ->setSetting('max_length', 10);

    $fields['strucnum'] = BaseFieldDefinition::create('string')
      ->setLabel('Номер строения')
      ->setSetting('max_length', 10);

    $fields['strstatus'] = BaseFieldDefinition::create('integer')
      ->setLabel('Признак строения');

    $fields['houseid'] = BaseFieldDefinition::create('string')
      ->setLabel('Уникальный идентификатор записи дома')
      ->setSetting('max_length', 36);

    $fields['houseguid'] = BaseFieldDefinition::create('string')
      ->setLabel('Глобальный уникальный идентификатор дома')
      ->setSetting('max_length', 36);

    $fields['aoguid'] = BaseFieldDefinition::create('string')
      ->setLabel('Guid записи родительского объекта (улицы, города, населенного пункта и т.п.)')
      ->setSetting('max_length', 36);

    $fields['statstatus'] = BaseFieldDefinition::create('integer')
      ->setLabel('Состояние дома');

    $fields['normdoc'] = BaseFieldDefinition::create('string')
      ->setLabel('Внешний ключ на нормативный документ')
      ->setSetting('max_length', 36);

    $fields['counter'] = BaseFieldDefinition::create('integer')
      ->setLabel('Счетчик записей домов для КЛАДР 4');

    static::setDefaultFieldsSettings($fields);

    return $fields;
  }

}
