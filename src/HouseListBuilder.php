<?php

/**
 * @file
 * Contains \Drupal\fias\HouseListBuilder.
 */

namespace Drupal\fias;

use Drupal\Component\Utility\SafeMarkup;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

/**
 * Defines a class to build a listing of fias house entities.
 *
 * @see \Drupal\fias\Entity\House
 */
class HouseListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = array(
      'guid' => $this->t('GUID'),
      'housenum' => 'Номер дома',
      'buildnum' => 'Номер корпуса',
      'strucnum' => 'Номер строения',
    );
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['guid'] = SafeMarkup::checkPlain($entity->get('houseguid')->value);
    $row['housenum'] = SafeMarkup::checkPlain($entity->get('housenum')->value);
    $row['buildnum'] = SafeMarkup::checkPlain($entity->get('buildnum')->value);
    $row['strucnum'] = SafeMarkup::checkPlain($entity->get('strucnum')->value);
    return $row + parent::buildRow($entity);
  }

}
