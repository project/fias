<?php

/**
 * @file
 * Contains \Drupal\fias\Component\FiasAddressParser.
 */

namespace Drupal\fias\Component;

/**
 * Provides helper functions to parse address strings to FIAS entities.
 */
class FiasAddressParser {

  /**
   * Parse house string and return FIAS formatted array.
   *
   * @param string $house
   * @return array|bool
   *   Array in format [
   *     'housenum' => '', // house number (номер дома), string 20
   *     'buildnum' => '', // building number (номер корпуса), string 10
   *     'strucnum' => '', // structure number (номер строения), string 10
   *   ];
   *   or bool if can not parse.
   */
  public static function parseHouse($house) {
    $exclude_charlist = [' ', '.', ',' ];

    $result = [
      'housenum' => '',
      'buildnum' => '',
      'strucnum' => '',
    ];

    $patterns = [
      '/^(\d+\s?([а-я]?|[\d\/]+))$/iu' => [1 => 'housenum'], // Только дом
      '/^(\d+\s?[а-йл-я]*)[^скорпу]*с[\D]*(\d+)$/iu' => [1 => 'housenum', 2 => 'strucnum'], // Дом и строение
      '/^(\d+\s?[а-йл-я]*)[^кстр]*кстр[\D]*(\d+)$/iu' => [1 => 'housenum', 2 => 'strucnum'],
      '/^(\d+\s?[а-йл-я]*)[^к]*к[^с]*(\d+|[а-я]+)$/iu' => [1 => 'housenum', 2 => 'buildnum'], // Дом и корпус
      '/^(\d+\s?[а-йл-я]*)[^к]*к\D*(\d+|[а-я]+)[^скорпу]*с[\D]*(\d+)$/iu' => [1 => 'housenum', 2 => 'buildnum', 3 => 'strucnum'],// Дом корпус и строение
      '/^(\d+\s?[а-йл-я]*)[^скорпу]*с[\D]*(\d+)[^к]*к[\D]*(\d+|[а-я]+)$/iu' => [1 => 'housenum', 2 => 'buildnum', 3 => 'strucnum'], // Дом строение и корпус
    ];

    $house = mb_strtoupper($house, 'UTF-8');
    $house = str_replace("ПУС", "", $house);
    $house = trim($house);

    $stop_words = [
      'ВРОДЕ', "НЕ", "СКАЗАЛ", "ПОТОМ", "НАВЕР", "ПОМНИ", "ЗВОН", "ЗАБЫЛ"
    ];

    foreach ($stop_words as $stop) {
      if (mb_strpos($house, $stop, 0, "UTF-8") !== FALSE) {
        return FALSE;
      }
    }

    $has_match = FALSE;
    foreach ($patterns as $pattern => $keys) {
      if ($has_match = preg_match($pattern, $house, $m)) {
        foreach ($keys as $key => $value) {
          $result[$value] = str_replace($exclude_charlist, '', $m[$key]);
        }
        break;
      }
    }

    return $has_match ? $result : FALSE;
  }

  /**
   * Return the normalized street name and street type.
   *
   * @param $street
   *   Raw street name.
   * @return array
   *   Array in the form: [
   *     'name' => normalized street name,
   *     'type' => the fias street type
   *   ]
   */
  public static function parseStreet($street) {
    $street = trim($street);
    $street_types = [
      'шоссе' => 'ш',
      'ш.' => 'ш',
      'ш' => 'ш',

      'улица' => 'ул',
      'ул.' => 'ул',
      'ул' => 'ул',

      'переулок' => 'пер',
      'пер.' => 'пер',
      'пер' => 'пер',

      'проспект' => 'пр-кт',
      'просп.' => 'пр-кт',
      'пр-кт' => 'пр-кт',
      'пр-т' => 'пр-кт',

      'проезд' => 'проезд',
      'пр-д' => 'проезд',
      'пр.' => 'проезд',

      'тупик' => 'туп',
      'туп.' => 'туп',
      'туп' => 'туп',

      'площадь' => 'пл',
      'пл.' => 'пл',
      'пл' => 'пл',

      'набережная' => 'наб',
      'наб.' => 'наб',
      'наб' => 'наб',

      'линия' => 'линия',

      'бульвар' => 'б-р',
      'бульв.' => 'б-р',
      'бул.' => 'б-р',
      'б-р' => 'б-р',

      'аллея' => 'аллея',
      'ал' => 'аллея',
      'алл' => 'аллея',
      'ал.' => 'аллея',
    ];

    $name_normalized = $street;
    $type_normalized = 'ул';

    foreach ($street_types as $k => $type) {
      $k = str_replace('.', '\.?\s?', $k);
      $pattern = "/^$k|$k$/isu";
      if (preg_match($pattern, $street, $matches)) {
        $name_normalized = preg_replace($pattern, '', $street);
        $type_normalized = $type;
        break;
      }
    }
    $name_normalized = trim($name_normalized);

    return [
      'name' => mb_convert_case($name_normalized, MB_CASE_TITLE, "UTF-8"),
      'type' => $type_normalized,
    ];
  }

}
