<?php

/**
 * @file
 * Contains \Drupal\fias\Component\FiasHelper.
 */

namespace Drupal\fias\Component;
use Drupal\fias\Entity\AddressObject;

/**
 * Provides helpers to operate with fias.
 */
class FiasHelper {

  /**
   * This function do the dirty hack for federal cities: add them to respective
   * regions.
   *
   * @param $region_guid
   *   The GUID of region.
   * @return NULL|\Drupal\fias\AddressObjectInterface
   *   The loaded federal city, that should belong to given region,
   *   but it doesn't.
   */
  public static function hackFederalCities($region_guid) {
    $city = NULL;

    switch ($region_guid) {
      // Московская область - город Москва
      case '29251dcf-00a1-4e34-98d4-5c47484a36d4':
        $city = AddressObject::loadByGuid('0c5b2444-70a0-4932-980c-b4dc0d3f02b5');
        break;

      // Ленинградская область - город Санкт-Петербург
      case '6d1ebb35-70c6-4129-bd55-da3969658f5d':
        $city = AddressObject::loadByGuid('c2deb16a-0330-4f05-821f-1d09c93331e6');
        break;

      // Республика Крым - город Севастополь
      case 'bd8e6511-e4b9-4841-90de-6bbc231a789e':
        $city = AddressObject::loadByGuid('6fdecb78-893a-4e3f-a5ba-aa062459463b');
        break;
    }

    return $city;
  }

  /**
   * Find xml files that match fias pattern.
   *
   * @param string $path_xml
   * @return bool|array
   */
  public static function findFiasXmlFiles($path_xml) {
    $files = glob($path_xml . '/*.{xml,XML}', GLOB_BRACE);

    if ($files === FALSE) {
      return FALSE;
    }

    foreach($files as &$file) {
      $file = static::parseXmlFilepath($file);
    }

    return $files;
  }

  /**
   * This function parse XML file filepath and return object with file
   * metainformation.
   *
   * @param string $filepath
   * @return object
   */
  public static function parseXmlFilepath($filepath) {
    $file = [
      'filepath' => $filepath,
      'filesize' => filesize($filepath),
      'prefix' => $filepath,
      'date' => '',
      'number' => '',
      'unknown' => FALSE,
    ];

    if (preg_match('/.*\/(\S+)_(\d+)_(\S+)\./', $filepath, $file_parts)) {
      $file['prefix'] = $file_parts[1];
      $file['date'] = $file_parts[2];
      $file['number'] = $file_parts[3];
    }

    $info = static::getXmlFilenamePrefixesWithInfo();
    if (isset($info[$file['prefix']])) {
      $file = array_merge($file, $info[$file['prefix']]);
    }
    else {
      $file['info'] = 'Неизвестный файл';
      $file['unknown'] = TRUE;
    }

    return (object)$file;
  }

  /**
   * Return fias files prefixes with info.
   *
   * @return array
   */
  public static function formatFilesize($filesize) {
    $decimal_points = 0;
    if ($filesize > 1024) {
      $filesize = $filesize / 1024 / 1024;
      if (floor($filesize) < 10) {
        $decimal_points = 3;
      }
    }
    return number_format($filesize, $decimal_points, '.', ' ');
  }

  /**
   * Return fias files prefixes with info.
   *
   * @return array
   */
  public static function getXmlFilenamePrefixesWithInfo() {
    return [
      'AS_ADDROBJ' => [
        'info' => 'Классификатор адресообразующих элементов (КЛАДЭ)',
        'vocabulary' => FALSE,
        'delete' => FALSE,
        'entity_type' => 'fias_address_object',
        'xml_node' => 'Object',
      ],
      'AS_HOUSE' => [
        'info' => 'Номера домов улиц городов и населенных пунктов, номера земельных участков и т.п',
        'vocabulary' => FALSE,
        'delete' => FALSE,
        'entity_type' => 'fias_house',
        'xml_node' => 'House',
      ],
      'AS_HOUSEINT' => [
        'info' => 'Интервалы домов',
        'vocabulary' => FALSE,
        'delete' => FALSE,
        'entity_type' => 'fias_houseint',
        'xml_node' => 'HouseInterval',
      ],
      'AS_LANDMARK' => [
        'info' => 'Описание мест расположения имущественных объектов',
        'vocabulary' => FALSE,
        'delete' => FALSE,
        'entity_type' => 'fias_landmark',
        'xml_node' => 'Landmark',
      ],
      'AS_NORMDOC' => [
        'info' => 'Нормативные документы, являющиеся основанием присвоения адресному элементу наименования',
        'vocabulary' => FALSE,
        'delete' => FALSE,
      ],
      'AS_SOCRBASE' => [
        'info' => 'Информация по типам адресных объектов.',
        'vocabulary' => FALSE,
        'delete' => FALSE,
      ],
      'AS_CURENTST' => [
        'info' => 'Информация по перечню статусов актуальности записи адресного элемента по классификатору КЛАДР4.0.',
        'vocabulary' => TRUE,
        'delete' => FALSE,
      ],
      'AS_ACTSTAT' => [
        'info' => 'Информация по перечню статусов актуальности записи  адресного элемента по ФИАС.',
        'vocabulary' => TRUE,
        'delete' => FALSE,
      ],
      'AS_OPERSTAT' => [
        'info' => 'Информация по перечню кодов операций над адресными объектами.',
        'vocabulary' => TRUE,
        'delete' => FALSE,
      ],
      'AS_CENTERST' => [
        'info' => 'Информация по статусу центра.',
        'vocabulary' => TRUE,
        'delete' => FALSE,
      ],
      'AS_INTVSTAT' => [
        'info' => 'Информация по перечню возможных значений интервалов домов.',
        'vocabulary' => TRUE,
        'delete' => FALSE,
      ],
      'AS_HSTSTAT' => [
        'info' => 'Информация по перечню возможных состояний объектов недвижимости.',
        'vocabulary' => TRUE,
        'delete' => FALSE,
      ],
      'AS_ESTSTAT' => [
        'info' => 'Информация по перечню возможных видов владений.',
        'vocabulary' => TRUE,
        'delete' => FALSE,
      ],
      'AS_STRSTAT' => [
        'info' => 'Информация по возможным видам строений',
        'vocabulary' => TRUE,
        'delete' => FALSE,
      ],
      'AS_DEL_ADDROBJ' => [
        'info' => 'Удалённые записи по адресообразующим элементам',
        'vocabulary' => FALSE,
        'delete' => TRUE,
      ],
      'AS_DEL_HOUSE' => [
        'info' => 'Удалённые записи с номерами домов улиц городов и населенных пунктов',
        'vocabulary' => FALSE,
        'delete' => TRUE,
      ],
      'AS_DEL_HOUSEINT' => [
        'info' => 'Удалённые записи с интервалами номеров домов улиц городов и населенных пунктов',
        'vocabulary' => FALSE,
        'delete' => TRUE,
      ],
      'AS_DEL_LANDMARK' => [
        'info' => 'Удалённые записям описания мест расположения имущественных объектов',
        'vocabulary' => FALSE,
        'delete' => TRUE,
      ],
      'AS_DEL_NORMDOC' => [
        'info' => 'Удалённые сведения по нормативным документам',
        'vocabulary' => FALSE,
        'delete' => TRUE,
      ],
    ];
  }

}
