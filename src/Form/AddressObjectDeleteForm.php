<?php

/**
 * @file
 * Contains \Drupal\fias\Form\AddressObjectDeleteForm.
 */

namespace Drupal\fias\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;
use Drupal\Core\Url;

/**
 * Provides a deletion confirmation form for the fias address object instance
 * deletion form.
 */
class AddressObjectDeleteForm extends ContentEntityDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.fias_address_object.collection');
  }

}
