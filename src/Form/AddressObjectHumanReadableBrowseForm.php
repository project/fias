<?php

/**
 * @file
 * Contains \Drupal\fias\Form\AddressObjectHumanReadableBrowseForm.
 */

namespace Drupal\fias\Form;

use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\fias\Component\FiasHelper;
use Drupal\fias\Entity\AddressObject;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * This form allow browse through address objects, on form: region-city-street.
 */
class AddressObjectHumanReadableBrowseForm extends FormBase {

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * Constructs a AddressObjectBrowseForm object.
   *
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager service.
   */
  public function __construct(EntityManagerInterface $entity_manager) {
    $this->entityManager = $entity_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fias_address_object_browse';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form_wrapper_id = 'fias-address-object-browse-form-wrapper';
    $form = [
      // Add wrapper to form
      '#prefix' => '<div id="' . $form_wrapper_id . '">',
      '#suffix' => '</div>',
    ];

    /** @var \Drupal\fias\AddressObjectStorageInterface $address_object_storage */
    $address_object_storage = $this->entityManager->getStorage('fias_address_object');

    $regions = $address_object_storage->getRegions('г');
    foreach ($regions as $guid => $region) {
      $region = $address_object_storage->loadByGuid($guid);
      $regions[$guid] = $region->label();
    }

    if ($form_state->hasValue('region')) {
      $region_default = $form_state->getValue('region');
    }
    else {
      reset($regions);
      list ($region_default, $region) = each($regions);
      reset($regions);
    }

    $form['region'] = [
      '#type' => 'select',
      '#title' => $this->t('Regions (@count)', ['@count' => count($regions)]),
      '#description' => $this->t('Regions are the top-level FIAS address objects.'),
      '#options' => $regions,
      '#default_value' => $region_default,
      '#weight' => 10,
      '#ajax' => [
        'callback' => [__CLASS__, 'updateRegions'],
        'wrapper' => $form_wrapper_id,
      ],
    ];


    $cities = $address_object_storage->getCitiesFromRegion($region_default);
    foreach ($cities as $guid => $region) {
      /** @var \Drupal\fias\Entity\AddressObject $city */
      $city = $address_object_storage->loadByGuid($guid);
      $cities[$guid] = empty($city) ? $guid : $city->label();
    }

    // Do the dirty hack with federal cities for the sake of human-readable
    // browse :)
    if ($city = FiasHelper::hackFederalCities($region_default)) {
      $cities = [$city->guid() => $city->label()] + $cities;
    }

    if ($form_state->hasValue('city')) {
      $city_default = $form_state->getValue('city');
    }
    else {
      reset($cities);
      list ($city_default, $city) = each($cities);
      reset($cities);
    }

    $form['city'] = [
      '#type' => 'select',
      '#title' => $this->t('City (@count)', ['@count' => count($cities)]),
      '#description' => $this->t('Cities in selected region above.'),
      '#options' => $cities,
      '#default_value' => $city_default,
      '#weight' => 20,
      '#ajax' => [
        'callback' => [__CLASS__, 'updateCity'],
        'wrapper' => $form_wrapper_id,
      ],
    ];

    if ($streets = $address_object_storage->getStreetsFromCity($city_default)) {
      foreach ($streets as $guid => $street) {
        /** @var \Drupal\fias\Entity\AddressObject $city */
        $street = $address_object_storage->loadByGuid($guid);
        $streets[$guid] = empty($street) ? $guid : $street->label();
      }

      if ($form_state->hasValue('street')) {
        $street_default = $form_state->getValue('street');
      }
      else {
        reset($streets);
        list ($street_default, $street) = each($streets);
        reset($streets);
      }

      $form['street'] = [
        '#type' => 'select',
        '#title' => $this->t('Street (@count)', ['@count' => count($streets)]),
        '#description' => $this->t('Streets in selected city above.'),
        '#options' => $streets,
        '#default_value' => $street_default,
        '#weight' => 30,
      ];
    }
    else {
      $city_default = AddressObject::loadByGuid($city_default);
      $form['street'] = [
        '#type' => 'markup',
        '#markup' => $this->t('There is no streets in @city', ['@city' => $city_default->label()]),
        '#weight' => 30,
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * AJAX callback for handling switching the regions selector.
   *
   * @param $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   * @return array
   *   The form array.
   */
  public static function updateRegions($form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * AJAX callback for handling switching the cities selector.
   *
   * @param $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   * @return array
   *   The form array.
   */
  public static function updateCity($form, FormStateInterface $form_state) {
    return $form;
  }

}
