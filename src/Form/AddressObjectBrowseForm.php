<?php

/**
 * @file
 * Contains \Drupal\fias\Form\AddressObjectBrowseForm.
 */

namespace Drupal\fias\Form;

use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * This form allow browse through address objects.
 */
class AddressObjectBrowseForm extends FormBase {

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * Constructs a AddressObjectBrowseForm object.
   *
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager service.
   */
  public function __construct(EntityManagerInterface $entity_manager) {
    $this->entityManager = $entity_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fias_address_object_browse';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form_wrapper_id = 'fias-address-object-browse-form-wrapper';
    $form = [
      // Add wrapper to form
      '#prefix' => '<div id="' . $form_wrapper_id . '">',
      '#suffix' => '</div>',
    ];

    /** @var \Drupal\fias\AddressObjectStorageInterface $address_object_storage */
    $address_object_storage = $this->entityManager->getStorage('fias_address_object');

    $address_objects = $address_object_storage->getRegions();
    foreach ($address_objects as $guid => $address_object) {
      $address_object = $address_object_storage->loadByGuid($guid);
      $address_objects[$guid] = $address_object->label();
    }

    if ($form_state->hasValue('region')) {
      $address_object_default = $form_state->getValue('region');
    }
    else {
      reset($address_objects);
      list ($address_object_default, $address_object) = each($address_objects);
      reset($address_objects);
    }

    $form['region'] = [
      '#type' => 'select',
      '#title' => $this->t('Regions (@count)', ['@count' => count($address_objects)]),
      '#description' => $this->t('Regions are the top-level FIAS address objects.'),
      '#options' => $address_objects,
      '#default_value' => $address_object_default,
      '#weight' => 10,
      '#ajax' => [
        'callback' => [__CLASS__, 'updateRegions'],
        'wrapper' => $form_wrapper_id,
      ],
    ];


    $address_objects = $address_object_storage->getChildren($address_object_default);
    foreach ($address_objects as $guid => $address_object) {
      $address_object = $address_object_storage->loadByGuid($guid);
      $address_objects[$guid] = $address_object->label();
    }

    if ($form_state->hasValue('area')) {
      $address_object_default = $form_state->getValue('area');
    }
    else {
      reset($address_objects);
      list ($address_object_default, $address_object) = each($address_objects);
      reset($address_objects);
    }

    $form['area'] = [
      '#type' => 'select',
      '#title' => $this->t('Areas (@count)', ['@count' => count($address_objects)]),
      '#description' => $this->t('Areas are the children of regions.'),
      '#options' => $address_objects,
      '#default_value' => $address_object_default,
      '#weight' => 20,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * AJAX callback for handling switching the regions selector.
   *
   * @param $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   * @return array
   *   The form array.
   */
  public static function updateRegions($form, FormStateInterface $form_state) {
    return $form;
  }

}
