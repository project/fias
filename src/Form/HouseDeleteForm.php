<?php

/**
 * @file
 * Contains \Drupal\fias\Form\HouseDeleteForm.
 */

namespace Drupal\fias\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;
use Drupal\Core\Url;

/**
 * Provides a deletion confirmation form for the fias house instance deletion
 * form.
 */
class HouseDeleteForm extends ContentEntityDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.fias_house.collection');
  }

}
