<?php

/**
 * @file
 * Contains \Drupal\fias\Form\SettingsForm.
 */

namespace Drupal\fias\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure fias settings.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fias_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('fias.settings');

    $form['path_xml'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path to XML files'),
      '#default_value' => $config->get('path_xml'),
      '#description' => $this->t('Path to the directory with FIAS XML files.'),
    ];
    $form['path_xsd'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path to XSD files'),
      '#default_value' => $config->get('path_xsd'),
      '#description' => $this->t('Path to the directory with FIAS XSD files.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('fias.settings')
      ->set('path_xml', $form_state->getValue('path_xml'))
      ->set('path_xsd', $form_state->getValue('path_xsd'))
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['fias.settings'];
  }

}
