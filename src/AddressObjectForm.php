<?php

/**
 * @file
 * Contains \Drupal\fias\AddressObjectForm.
 */

namespace Drupal\fias;


use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides form for fias address object instance forms.
 */
class AddressObjectForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $this->entity->save();

    if ($this->entity->isNew()) {
      $message = $this->t('Added an FIAS address object %label.', array('%label' => $this->entity->label()));
    }
    else {
      $message = $this->t('The address %label has been updated.', array('%label' => $this->entity->label()));
    }
    drupal_set_message($message);

    $form_state->setRedirect('entity.fias_address_object.collection');
  }

}
