<?php

/**
 * @file
 * Contains \Drupal\fias\Controller\XmlFilesController.
 */

namespace Drupal\fias\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\fias\Component\FiasHelper;

/**
 * Returns some info for xml files founded in xml directory from settings.
 */
class XmlFilesController extends ControllerBase {

  /**
   * @return array
   */
  public function xmlFiles() {
    $config = $this->config('fias.settings');
    $path_xml = $config->get('path_xml');
    $table = [
      '#type' => 'table',
      '#caption' => $this->t('Files'),
      '#empty' => $this->t('There is no files in directory %dir.', ['%dir' => $path_xml]),
      '#header' => [
        $this->t('Prefix'),
        $this->t('Date'),
        $this->t('Number'),
        $this->t('Filesize'),
        $this->t('Info'),
        $this->t('Operations'),
      ],
      '#rows' => [],
    ];
    $rows = &$table['#rows'];

    $files = FiasHelper::findFiasXmlFiles($path_xml);

    if ($files === FALSE) {
      $table['#empty'] = $this->t('Wrong directory %dir!', ['%dir' => $path_xml]);
    }

    foreach($files as $file) {
      $filesize = $this->t('@filesize Mb', ['@filesize' => FiasHelper::formatFilesize($file->filesize)]);
      $rows[] = [$file->prefix, $file->date, $file->number, $filesize, $file->info, ''];
    }

    return $table;
  }

}
