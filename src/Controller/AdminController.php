<?php

/**
 * @file
 * Contains \Drupal\fias\Controller\AdminController.
 */

namespace Drupal\fias\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;

/**
 * Returns some admin info.
 */
class AdminController extends ControllerBase {

  /**
   * Controller for FIAS admin overview page.
   *
   * @return array
   *   The build array.
   */
  public function adminOverview() {
    return array(
      'entities_links' => [
        '#theme' => 'item_list',
        '#title' => $this->t('Control FIAS entities'),
        '#items' => [
          'fias_address_object' => [
            '#type' => 'link',
            '#title' => $this->t('Address Object'),
            '#url' => Url::fromRoute('entity.fias_address_object.collection'),
          ],
          'fias_house' => [
            '#type' => 'link',
            '#title' => $this->t('House'),
            '#url' => Url::fromRoute('entity.fias_house.collection'),
          ],
          'fias_houseint' => [
            '#type' => 'link',
            '#title' => $this->t('House Interval'),
            '#url' => Url::fromRoute('entity.fias_houseint.collection'),
          ],
        ],
      ],
      'other_links' => [
        '#theme' => 'item_list',
        '#title' => $this->t('Other FIAS links'),
        '#items' => [
          'fias_browse_address_object' => [
            '#type' => 'link',
            '#title' => $this->t('Browse Address Objects'),
            '#url' => Url::fromRoute('fias.browse_address_object'),
          ],
          'fias_human_readable_browse_address_object' => [
            '#type' => 'link',
            '#title' => $this->t('Human Readable Browse Address Objects'),
            '#url' => Url::fromRoute('fias.human_readable_browse_address_object'),
          ],
        ],
      ],
    );
  }

}
