<?php

/**
 * @file
 * Contains \Drupal\fias\HouseInterface.
 */

namespace Drupal\fias;

use Drupal\Core\Entity\ContentEntityInterface;

interface HouseInterface extends ContentEntityInterface {

}
