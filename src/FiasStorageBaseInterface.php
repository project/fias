<?php

/**
 * @file
 * Contains \Drupal\fias\FiasStorageBaseInterface.
 */

namespace Drupal\fias;

use Drupal\Core\Entity\EntityStorageInterface;

interface FiasStorageBaseInterface extends EntityStorageInterface {

  /**
   * Return actual address object for given guid.
   *
   * @param string $aoguid
   *   The address object guid.
   * @return \Drupal\fias\AddressObjectInterface|null
   *   The actual (currstatus = 0) address object for given guid.
   */
  public function loadByGuid($aoguid);

}
