<?php

/**
 * @file
 * Contains \Drupal\fias\HouseintStorageInterface.
 */

namespace Drupal\fias;

use Drupal\Core\Entity\EntityStorageInterface;

interface HouseintStorageInterface extends EntityStorageInterface {

}
