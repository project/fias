<?php

/**
 * @file
 * Contains \Drupal\fias\AddressObjectStorage.
 */

namespace Drupal\fias;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

/**
 * Defines a Storage class for fias address objects.
 */
class AddressObjectStorage extends SqlContentEntityStorage implements AddressObjectStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function loadByGuid($aoguid) {
    // @todo Create static cache, like in EntityStorageBase::load()
    // @todo Create self::loadByGuidMultiple()
    $query = $this->getQueryBase()
      ->condition('aoguid', $aoguid);
    $query->addField('fao', 'aoid');
    $result = $query->execute()->fetchAssoc();
    return $this->load($result['aoid']);
  }

  /**
   * {@inheritdoc}
   */
  public function getAllParents($aoguid) {
    $parents = [];

    while ($aoguid) {
      $addrobj = $this->getQueryBase()
        ->condition('aoguid', $aoguid)
        ->execute()
        ->fetchAssoc();
      if ($addrobj['parentguid']) {
        $parents[] = $addrobj['parentguid'];
        $aoguid = $addrobj['parentguid'];
      }
      else {
        $aoguid = '';
      }
    }

    return $parents;
  }

  /**
   * {@inheritdoc}
   */
  public function getChildren($aoguid, $pattern = '') {
    $query = $this->getQueryBase()
      ->condition('parentguid', $aoguid);

    if (!empty($pattern)) {
      $query->condition('formalname', '%' . $pattern . '%', 'LIKE');
    }

    $children = [];
    foreach ($query->execute() as $row) {
      $children[$row->aoguid] = $row;
    }

    return $children;
  }

  /**
   * {@inheritdoc}
   */
  public function getRegions($exclude_shortname = '') {
    $query = $this->getQueryBase()
      ->condition('aolevel', static::LEVEL_REGION)
      ->isNull('parentguid');

    if (!empty($exclude_shortname)) {
      if (is_string($exclude_shortname)) {
        $exclude_shortname = [$exclude_shortname];
      }
      if (is_array($exclude_shortname)) {
        $query->condition('shortname', $exclude_shortname, 'NOT IN');
      }
    }

    $regions = [];
    foreach ($query->execute() as $row) {
      $regions[$row->aoguid] = $row;
    }

    return $regions;
  }

  /**
   * {@inheritdoc}
   */
    public function getAreasFromRegion($region_guid) {
    $query = $this->getQueryBase()
      ->condition('parentguid', $region_guid)
      ->condition('aolevel', static::LEVEL_AREA);

    $areas = [];
    foreach ($query->execute() as $row) {
      $areas[$row->aoguid] = $row;
    }

    return $areas;
  }

  /**
   * {@inheritdoc}
   */
  public function getPossibleCityParentsGuidsByRegionGuid($region_guid) {
    $areas = $this->getAreasFromRegion($region_guid);
    $parents = array_keys($areas);

    // Add the region itself to the array of possible city parents
    $parents[] = $region_guid;

    return $parents;
  }

  /**
   * {@inheritdoc}
   */
  public function getCitiesFromRegion($region_guid, $pattern = '') {
    $parents = $this->getPossibleCityParentsGuidsByRegionGuid($region_guid);
    $query = $this->getQueryBase()
      ->condition('parentguid', $parents, 'IN')
      ->condition('aolevel', [static::LEVEL_CITY, static::LEVEL_LOCALITY], 'IN');

      if (!empty($pattern)) {
        $query->condition('formalname', '%' . $pattern . '%', 'LIKE');
      }

    $cities = [];
    foreach ($query->execute() as $row) {
      $cities[$row->aoguid] = $row;
    }

    return $cities;
  }

  /**
   * {@inheritdoc}
   */
  public function getStreetsFromCity($city_guid, $pattern = '') {
    $query = $this->getQueryBase()
      ->condition('parentguid', $city_guid)
      ->condition('aolevel', static::LEVEL_STREET);

    if (!empty($pattern)) {
      $query->condition('formalname', '%' . $pattern . '%', 'LIKE');
    }

    $cities = [];
    foreach ($query->execute() as $row) {
      $cities[$row->aoguid] = $row;
    }

    return $cities;
  }

  /**
   * This function return the query billet.
   *
   * @return \Drupal\Core\Database\Query\SelectInterface
   */
  private function getQueryBase() {
    return $this->database->select('fias_addrobj', 'fao')
      ->fields('fao', ['aoguid', 'shortname', 'offname', 'formalname', 'aolevel', 'parentguid'])
      ->condition('currstatus', 0)
      ->orderBy('offname', 'ASC');
  }

}
