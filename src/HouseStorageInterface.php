<?php

/**
 * @file
 * Contains \Drupal\fias\HouseStorageInterface.
 */

namespace Drupal\fias;

use Drupal\Core\Entity\EntityStorageInterface;

interface HouseStorageInterface extends EntityStorageInterface {

  /**
   * Return houses for given address object and optionally limited by housenum,
   * buildnum, strucnum.
   *
   * @param string $address_object_guid
   *   The fias address object guid.
   * @param array $house
   *   Array with following structure: array(
   *     'housenum' => 1А, // can not be empty
   *     'buildnum' => 4,  // can be empty
   *     'strucnum' => 1,  // can be empty
   *   )
   * @return array
   *   Array with found houses, keyed by GUID and sorted ascending by housenum,
   *   buildnum, strucnum.
   */
  public function getHouses($address_object_guid, $house = []);

}
