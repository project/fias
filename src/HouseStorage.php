<?php

/**
 * @file
 * Contains \Drupal\fias\HouseStorage.
 */

namespace Drupal\fias;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

class HouseStorage extends SqlContentEntityStorage implements HouseStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function getHouses($address_object_guid, $house = []) {
    $query = $this->database->select('fias_house', 'fh')
      ->fields('fh', ['houseguid', 'housenum', 'buildnum', 'strucnum', 'strstatus'])
      ->condition('aoguid', $address_object_guid)
      ->orderBy('housenum', 'ASC')
      ->orderBy('buildnum', 'ASC')
      ->orderBy('strucnum', 'ASC');

    if (!empty($house)) {
      $this->addCondition($query, 'housenum', $house['housenum']);
      $this->addCondition($query, 'buildnum', $house['buildnum'], TRUE);
      $this->addCondition($query, 'strucnum', $house['strucnum'], TRUE);
    }

    $houses = [];
    foreach ($query->execute() as $row) {
      $houses[$row->houseguid] = $row;
    }

    return $houses;
  }

  /**
   * Add condition for value, case sensitive.
   *
   * @param $query
   *   The query add condition to.
   * @param string $field
   *   The field name.
   * @param string $value
   *   Value to search. Can not be empty.
   * @param bool $addIsNull
   *   Flag indicates that we should add "IS NULL" to query, if value is empty.
   * @return \Drupal\Core\Database\Query\SelectInterface
   *   Query with added condition to it (or condition is not added, because
   * value is empty, or added is null condition).
   */
  private function addCondition($query, $field, $value, $addIsNull = FALSE) {
    if (!empty($value)) {
      $value_low = mb_strtolower($value);
      $value_up = mb_strtoupper($value);

      if ($value_low === $value_up) {
        $query->condition($field, $value_low);
      }
      else {
        $or_condition_group = $query->orConditionGroup()
          ->condition($field, $value_low)
          ->condition($field, $value_up);
        $query->condition($or_condition_group);
      }
    }
    elseif ($addIsNull) {
      $query->isNull($field);
    }
    return $query;
  }

}
