<?php

/**
 * @file
 * Contains \Drupal\fias\AddressObjectStorageInterface.
 */

namespace Drupal\fias;

/**
 * Defines an interface for fias address object entity storage classes.
 */
interface AddressObjectStorageInterface extends FiasStorageBaseInterface {

  /**
   * Уровень адресного объекта: регион (субъект российской федерации).
   *
   * @var int
   */
  const LEVEL_REGION = 1;

  /**
   * Уровень адресного объекта: район.
   *
   * @var int
   */
  const LEVEL_AREA = 3;

  /**
   * Уровень адресного объекта: город.
   *
   * @var int
   */
  const LEVEL_CITY = 4;

  /**
   * Уровень адресного объекта: внутригородская территория.
   *
   * @var int
   */
  const LEVEL_CITY_AREA = 5;

  /**
   * Уровень адресного объекта: населенный пункт.
   *
   * @var int
   */
  const LEVEL_LOCALITY = 6;

  /**
   * Уровень адресного объекта: улица.
   *
   * @var int
   */
  const LEVEL_STREET = 7;

  /**
   * Уровень адресного объекта: дополнительная территория (ГСК, СНТ, лагери отдыха и т.п.).
   *
   * @var int
   */
  const LEVEL_ADDITIONAL_AREA = 90;

  /**
   * Уровень адресного объекта: улицы на дополнительной территории (улицы, линии, проезды)
   *
   * @var int
   */
  const LEVEL_ADDITIONAL_AREA_STREET = 91;

  /**
   * Return all parents guids.
   *
   * @param string $aoguid
   *   The GUID of address object.
   * @return string[]
   *   Array of parents GUIDs for given $aoguid.
   */
  public function getAllParents($aoguid);

  /**
   * Return all direct children address objects matched the pattern if given.
   *
   * @param $aoguid
   *   The GUID of address object.
   * @param $pattern
   *   The substring to search for. Do not use '%', because they are inserted
   *   automatically to the SQL query.
   * @return array
   *   Array of direct children for given $aoguid, keyed by GUID, that match for
   *   pattern if given.
   */
  public function getChildren($aoguid, $pattern = '');

  /**
   * Return all regions (Region - is an address object with
   * aolevel = LEVEL_REGION), in alphabetic order.
   *
   * @param string|string[] $exclude_shortname
   *   Address objects with given shortnames will be excluded from result. This
   *   filter is disabled by default.
   * @return array Array with regions, keyed by GUID.
   *   Array with regions, keyed by GUID.
   */
  public function getRegions($exclude_shortname = '');


  /**
   * Return all areas (area - is an address object with aolevel = LEVEL_AREA)
   * in given region.
   *
   * @param $region_guid
   *   The region GUID.
   * @return array
   *   Array with areas, keyed by GUID.
   */
  public function getAreasFromRegion($region_guid);

  /**
   * Return all cities (city - is an address object with aolevel = LEVEL_CITY)
   * in given region (region - is an address object with aolevel = LEVEL_REGION).
   *
   * Note that federal cities (with aolevel = 1) are not belong to any region,
   * and will never returned by this function.
   *
   * @param $region_guid
   *   The region GUID.
   * @param $pattern
   *   The substring to search for. Do not use '%', because they are inserted
   *   automatically to the SQL query.
   * @return array
   *   Array with found cities, keyed by GUID.
   */
  public function getCitiesFromRegion($region_guid, $pattern = '');

  /**
   * Return all streets (street - is an address object with aolevel = LEVEL_STREET)
   * in given region (region - is an address object with aolevel = LEVEL_REGION).
   *
   * @param $city_guid
   *   The city GUID.
   * @param $pattern
   *   The substring to search for. Do not use '%', because they are inserted
   *   automatically to the SQL query.
   * @return array
   *   Array with found streets, keyed by GUID.
   */
  public function getStreetsFromCity($city_guid, $pattern = '');

  /**
   * Return array of GUIDs, that can be parents for cities.
   *
   * @param $region_guid
   *   The region GUID.
   * @return string[]
   *   Array of GUIDs.
   */
  public function getPossibleCityParentsGuidsByRegionGuid($region_guid);

}
