<?php

/**
 * @file
 * Contains \Drupal\fias\AddressObjectListBuilder.
 */

namespace Drupal\fias;

use Drupal\Component\Utility\SafeMarkup;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

/**
 * Defines a class to build a listing of fias address object entities.
 *
 * @see \Drupal\fias\Entity\AddressObject
 */
class AddressObjectListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = array(
      'GUID' => $this->t('GUID'),
      'formalname' => $this->t('Name'),
      'aolevel' => $this->t('Level'),
      'currstatus' => $this->t('Status'),
    );
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['GUID'] = SafeMarkup::checkPlain($entity->get('aoguid')->value);
    $row['label'] = $entity->label();
    $row['aolevel'] = SafeMarkup::checkPlain($entity->get('aolevel')->value);
    $row['currstatus'] = SafeMarkup::checkPlain($entity->get('currstatus')->value);
    return $row + parent::buildRow($entity);
  }

}
