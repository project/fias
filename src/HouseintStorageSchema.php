<?php

/**
 * @file
 * Contains \Drupal\fias\HouseintStorageSchema.
 */

namespace Drupal\fias;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorageSchema;

class HouseintStorageSchema extends SqlContentEntityStorageSchema {

  /**
   * {@inheritdoc}
   */
  protected function getEntitySchema(ContentEntityTypeInterface $entity_type, $reset = FALSE) {
    $schema = parent::getEntitySchema($entity_type, $reset);

    $schema['fias_houseint']['indexes'] += array(
      'intguid' => array('intguid'),
    );

    return $schema;
  }

}
