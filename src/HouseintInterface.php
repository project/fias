<?php

/**
 * @file
 * Contains \Drupal\fias\HouseintInterface.
 */

namespace Drupal\fias;

use Drupal\Core\Entity\ContentEntityInterface;

interface HouseintInterface extends ContentEntityInterface {

}
