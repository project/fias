<?php

/**
 * @file
 * Contains \Drupal\fias\HouseStorageSchema.
 */

namespace Drupal\fias;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorageSchema;

class HouseStorageSchema extends SqlContentEntityStorageSchema {

  /**
   * {@inheritdoc}
   */
  protected function getEntitySchema(ContentEntityTypeInterface $entity_type, $reset = FALSE) {
    $schema = parent::getEntitySchema($entity_type, $reset);

    $schema['fias_house']['indexes'] += array(
      'houseguid' => array('houseguid'),
    );

    return $schema;
  }

}
