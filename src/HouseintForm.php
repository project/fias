<?php

/**
 * @file
 * Contains \Drupal\fias\HouseintForm.
 */

namespace Drupal\fias;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides form for fias house interval instance forms.
 */
class HouseintForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $this->entity->save();

    if ($this->entity->isNew()) {
      $message = $this->t('Added an FIAS house interval %label.', array('%label' => $this->entity->label()));
    }
    else {
      $message = $this->t('The FIAS house interval %label has been updated.', array('%label' => $this->entity->label()));
    }
    drupal_set_message($message);

    $form_state->setRedirect('entity.fias_houseint.collection');
  }

}
