<?php

/**
 * @file
 * FIAS module drush integration.
 */

use Drupal\fias\Component\FiasHelper;

/**
 * Implements hook_drush_command().
 */
function fias_drush_command() {
  $items = array();

  $items['fias-xml'] = array(
    'description' => dt('Show FIAS XML files information'),
    'examples' => array(
      'drush fias-xml' => dt('Show FIAS XML files information'),
    ),
    'core' => array('8+'),
    'aliases' => array('fxml'),
    'arguments' => array(
      'path_xml' => dt('The path to XML files.'),
    ),
    'options' => array(
      'filepath' => dt('Show filepath instead of parsed file name elements.'),
    ),
    'callback' => 'drush_fias_xml',
  );

  $items['fias-xml-import'] = array(
    'description' => dt('Import FIAS XML file.'),
    'examples' => array(
      'drush fias-xml-import' => dt('Import FIAS XML file.'),
    ),
    'core' => array('8+'),
    'aliases' => array('fxmli'),
    'arguments' => array(
      'filepath' => dt('The XML file filepath.'),
    ),
    'options' => array(
      'count_uniques' => dt('Count uniques nodes on import or not. Default is disabled, because memory consumption would be too big.'),
      'refresh_rate' => dt('How often print information to console and reset storage caches. Default is 100. Lower values can avoid to memory leak on Drupal 8 beta 12.'),
    ),
    'callback' => 'drush_fias_xml_import',
  );

  return $items;
}

/**
 * Executes a fias-xml command.
 *
 * @param string $path_xml
 *  The path to XML files.
 * @return void
 */
function drush_fias_xml($path_xml = '') {
  if (empty($path_xml)) {
    $path_xml = \Drupal::service('config.factory')->get('fias.settings')->get('path_xml');
    drush_print(dt('Path to xml files readed from config: @path_xml', ['@path_xml' => $path_xml]));
    drush_print();
  }

  $files = FiasHelper::findFiasXmlFiles($path_xml);

  if ($files === FALSE) {
    return drush_set_error(dt('Some error happens while get FIAS files.'));
  }

  $filepath = drush_get_option('filepath', FALSE);

  // Generate headers
  if ($filepath) {
    $rows[] = [dt('Filepath'), dt('Filesize'), dt('Info')];
  }
  else {
    $rows[] = [dt('Prefix'), dt('Date'), dt('Number'), dt('Filesize'), dt('Info')];
  }

  // Generate rows
  foreach($files as $file) {
    if ($filepath) {
      $rows[] = [$file->filepath, dt('@filesize Mb', ['@filesize' => FiasHelper::formatFilesize($file->filesize)]), $file->info];
    }
    else {
      $rows[] = [$file->prefix, $file->date, $file->number, dt('@filesize Mb', ['@filesize' => FiasHelper::formatFilesize($file->filesize)]), $file->info];
    }
  }

  drush_print_table($rows, TRUE);
}

/**
 * Executes a fias-xml-import command.
 *
 * @param string $filepath
 *  The path to XML files.
 * @return bool|void
 */
function drush_fias_xml_import($filepath = '') {
  if (empty($filepath)) {
    return drush_set_error(dt('The XML filepath is required.'));
  }

  $file = FiasHelper::parseXmlFilepath($filepath);

  if ($file->unknown) {
    return drush_set_error(dt('The XML filename signature is unknown.'));
  }
  elseif (!isset($file->entity_type)) {
    return drush_set_error(dt('This file has no corresponded entity type to import.'));
  }

  $counter_total = 0;
  $counter_new = 0;
  $counter_updated = 0;
  $memory_start = number_format(memory_get_usage(TRUE) / 1024 / 1024, 1, '.', ' ');
  $guids = [];
  $unique = 0;
  $counter_reset_cache = 0;

  $count_uniques = drush_get_option('count_uniques', FALSE);
  $refresh_rate = drush_get_option('refresh_rate', 100);

  /** @var  Drupal\Core\Entity\EntityStorageInterface $entity_storage */
  $entity_storage = Drupal::service('entity.manager')->getStorage($file->entity_type);
  $reader = new XMLReader();
  $reader->open($filepath);
  while ($reader->read()) {
    if ($reader->name === $file->xml_node && $reader->nodeType == XMLReader::ELEMENT) {
      $counter_total++;
      $node = [];
      foreach ($reader->expand()->attributes as $attribute) {
        $node[strtolower($attribute->name)] = $attribute->value;
      }

      /** @var \Drupal\fias\Entity\FiasEntityBase $entity */
      $entity = $entity_storage->create($node);

      switch ($entity->save()) {
        case SAVED_NEW:
          $counter_new++;
          break;
        case SAVED_UPDATED:
          $counter_updated++;
          break;
      }

      if ($count_uniques) {
        if (isset($guids[$entity->guid()])) {
          $guids[$entity->guid()]++;
        }
        else {
          $guids[$entity->guid()] = 0;
        }
        $unique = count($guids);
      }

      // Entity storage can blow up with caches so clear them out.
      // Also, dt() (translations) are leak on Drupal 8 beta 12.
      if (++$counter_reset_cache > $refresh_rate) {
        $counter_reset_cache = 0;
        drupal_static_reset();
        $entity_storage->resetCache();

        $time = time() - REQUEST_TIME;

        drush_print(dt("Total/Unique: @total/@unique (@peru%) New/Updated: @new/@upd Speed: @speed nodes/sec Time: @time sec Memory(on start/current): (@mstart/@mcur) Mb \r", [
          '@total' => number_format($counter_total, 0, '', ' '),
          '@unique' => number_format($unique, 0, '', ' '),
          '@peru' => number_format(($unique/$counter_total)*100, 1, '.', ' '),
          '@new' => number_format($counter_new, 0, '', ' '),
          '@upd' => number_format($counter_updated, 0, '', ' '),
          '@speed' => number_format($counter_total / ($time + 1), 0, '', ' '),
          '@time' => number_format($time, 0, '', ' '),
          '@mstart' => $memory_start,
          '@mcur' => number_format(memory_get_usage(TRUE) / 1024 / 1024, 1, '.', ' '),
        ]), 0, NULL, FALSE);
      }
    }
  }

  drush_print(dt("\nImport finished."));

  $reader->close();
}
