<?php

/**
 * @file
 * Contains \Drupal\Tests\fias\Unit\FiasAddressParserTest.
 */

namespace Drupal\Tests\fias\Unit;

use Drupal\fias\Component\FiasAddressParser;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\fias\Component\FiasAddressParser
 * @group fias
 */
class FiasAddressParserTest extends UnitTestCase {

  /**
   * Tests a static FiasAddressParser::parseHouse method.
   */
  public function testParseHouse() {
    $expected = [
      //h(\d)
      '28' => ['housenum' => '28', 'buildnum' => '', 'strucnum' => ''],
      '28/88' => ['housenum' => '28/88', 'buildnum' => '', 'strucnum' => ''],

      //h(\d\b)
      '28А' => ['housenum' => '28А', 'buildnum' => '', 'strucnum' => ''],
      '28 А' => ['housenum' => '28А', 'buildnum' => '', 'strucnum' => ''],

      //WTF
      '28 ДЖИГУРДА ВОЙНА забыл' => FALSE,
      '28А ДЖИГУРДА ВОЙНА забыл' => FALSE,
      '28 А ДЖИГУРДА ВОЙНА забыл' => FALSE,
      'Я танцую корпус 10' => FALSE,

      //WTF h(\d\b) b(\d)
      '20 А корпус вроде 15' => FALSE,

      //WTF h(\d\b) b(\b)
      '20 А корпус вроде А' => FALSE,

      '28 корпус 1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28 корпус. 1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28 КОРПУС 1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28 КОРПУС. 1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28 корп 1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28 корп. 1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28 КОРП 1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28 КОРП. 1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28 кор 1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28 кор. 1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28 КОР 1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28 КОР. 1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28 кк 1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28 кк. 1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28 КК 1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28 КК. 1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28 к 1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28 к. 1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28 К 1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28 К. 1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],

      //h(\d\b) b(\d)
      '28А корпус 1' => ['housenum' => '28А', 'buildnum' => '1', 'strucnum' => ''],
      '28А корп 1' => ['housenum' => '28А', 'buildnum' => '1', 'strucnum' => ''],
      '28А кор 1' => ['housenum' => '28А', 'buildnum' => '1', 'strucnum' => ''],
      '28А кк 1' => ['housenum' => '28А', 'buildnum' => '1', 'strucnum' => ''],
      '28А к 1' => ['housenum' => '28А', 'buildnum' => '1', 'strucnum' => ''],

      '28 А корпус 1' => ['housenum' => '28А', 'buildnum' => '1', 'strucnum' => ''],
      '28 А корп 1' => ['housenum' => '28А', 'buildnum' => '1', 'strucnum' => ''],
      '28 А кор 1' => ['housenum' => '28А', 'buildnum' => '1', 'strucnum' => ''],
      '28 А кк 1' => ['housenum' => '28А', 'buildnum' => '1', 'strucnum' => ''],
      '28 А к 1' => ['housenum' => '28А', 'buildnum' => '1', 'strucnum' => ''],

      '28 Акорпус 1' => ['housenum' => '28А', 'buildnum' => '1', 'strucnum' => ''],
      '28 Акорп 1' => ['housenum' => '28А', 'buildnum' => '1', 'strucnum' => ''],
      '28 Акор 1' => ['housenum' => '28А', 'buildnum' => '1', 'strucnum' => ''],
      '28 Акк 1' => ['housenum' => '28А', 'buildnum' => '1', 'strucnum' => ''],
      '28 Ак 1' => ['housenum' => '28А', 'buildnum' => '1', 'strucnum' => ''],

      '28корпус 1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28корпус. 1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28КОРПУС 1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28КОРПУС. 1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28корп 1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28корп. 1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28КОРП 1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28КОРП. 1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28кор 1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28кор. 1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28КОР 1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28КОР. 1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28кк 1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28кк. 1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28КК 1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28КК. 1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28к 1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28к. 1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28К 1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28К. 1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],

      '28 корпус1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28 корпус.1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28 КОРПУС1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28 КОРПУС.1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28 корп1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28 корп.1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28 КОРП1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28 КОРП.1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28 кор1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28 кор.1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28 КОР1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28 КОР.1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28 кк1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28 кк.1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28 КК1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28 КК.1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28 к1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28 к.1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28 К1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28 К.1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],

      '28корпус1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28корпус.1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28КОРПУС1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28КОРПУС.1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28корп1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28корп.1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28КОРП1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28КОРП.1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28кор1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28кор.1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28КОР1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28КОР.1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28кк1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28кк.1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28КК1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28КК.1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28к1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28к.1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28К1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],
      '28К.1' => ['housenum' => '28', 'buildnum' => '1', 'strucnum' => ''],

      '28 корпус А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28 корпус. А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28 КОРПУС А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28 КОРПУС. А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28 корп А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28 корп. А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28 КОРП А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28 КОРП. А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28 кор А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28 кор. А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28 КОР А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28 КОР. А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28 кк А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28 кк. А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28 КК А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28 КК. А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28 к А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28 к. А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28 К А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28 К. А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],

      '28 корпусА' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28 корпус.А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28 КОРПУСА' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28 КОРПУС.А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28 корпА' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28 корп.А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28 КОРПА' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28 КОРП.А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28 корА' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28 кор.А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28 КОРА' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28 КОР.А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28 ккА' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28 кк.А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28 ККА' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28 КК.А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28 кА' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28 к.А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28 КА' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28 К.А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],

      '28корпус А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28корпус. А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28КОРПУС А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28КОРПУС. А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28корп А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28корп. А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28КОРП А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28КОРП. А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28кор А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28кор. А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28КОР А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28КОР. А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28кк А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28кк. А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28КК А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28КК. А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28к А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28к. А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28К А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28К. А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],

      '28корпусА' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28корпус.А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28КОРПУСА' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28КОРПУС.А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28корпА' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28корп.А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28КОРПА' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28КОРП.А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28корА' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28кор.А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28КОРА' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28КОР.А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28ккА' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28кк.А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28ККА' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28КК.А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28кА' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28к.А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28КА' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],
      '28К.А' => ['housenum' => '28', 'buildnum' => 'А', 'strucnum' => ''],

      '28аК.А' => ['housenum' => '28А', 'buildnum' => 'А', 'strucnum' => ''],
      '28АК.1' => ['housenum' => '28А', 'buildnum' => '1', 'strucnum' => ''],
      '28 а К.1' => ['housenum' => '28А', 'buildnum' => '1', 'strucnum' => ''],
      '28 А К. 1' => ['housenum' => '28А', 'buildnum' => '1', 'strucnum' => ''],

      '29 строение 1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29 строение. 1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29 СТРОЕНИЕ 1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29 СТРОЕНИЕ. 1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29 кстр 1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29 кстр. 1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29 КСТР 1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29 КСТР. 1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29 стр 1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29 стр. 1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29 СТР 1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29 СТР. 1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29 с 1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29 с. 1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29 С 1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29 С. 1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],

      '29строение 1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29строение. 1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29СТРОЕНИЕ 1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29СТРОЕНИЕ. 1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29кстр 1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29кстр. 1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29КСТР 1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29КСТР. 1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29стр 1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29стр. 1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29СТР 1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29СТР. 1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29с 1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29с. 1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29С 1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29С. 1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],

      '29 строение1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29 строение.1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29 СТРОЕНИЕ1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29 СТРОЕНИЕ.1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29 кстр1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29 кстр.1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29 КСТР1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29 КСТР.1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29 стр1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29 стр.1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29 СТР1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29 СТР.1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29 с1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29 с.1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29 С1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29 С.1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],

      '29строение1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29строение.1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29СТРОЕНИЕ1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29СТРОЕНИЕ.1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29кстр1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29кстр.1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29КСТР1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29КСТР.1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29стр1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29стр.1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29СТР1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29СТР.1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29с1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29с.1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29С1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],
      '29С.1' => ['housenum' => '29', 'buildnum' => '', 'strucnum' => '1'],

      '30 корпус 1 строение 2' => ['housenum' => '30', 'buildnum' => '1', 'strucnum' => '2'],
      '30 корпус 1 стр 2' => ['housenum' => '30', 'buildnum' => '1', 'strucnum' => '2'],
      '30 корпус 1 с 2' => ['housenum' => '30', 'buildnum' => '1', 'strucnum' => '2'],

      '30 корпус. 1 строение. 2' => ['housenum' => '30', 'buildnum' => '1', 'strucnum' => '2'],
      '30 корпус. 1 стр. 2' => ['housenum' => '30', 'buildnum' => '1', 'strucnum' => '2'],
      '30 корпус. 1 с. 2' => ['housenum' => '30', 'buildnum' => '1', 'strucnum' => '2'],

      '30 корп 1 строение 2' => ['housenum' => '30', 'buildnum' => '1', 'strucnum' => '2'],
      '30 корп 1 стр 2' => ['housenum' => '30', 'buildnum' => '1', 'strucnum' => '2'],
      '30 корп 1 с 2' => ['housenum' => '30', 'buildnum' => '1', 'strucnum' => '2'],

      '30 корп. 1 строение 2' => ['housenum' => '30', 'buildnum' => '1', 'strucnum' => '2'],
      '30 корп. 1 стр 2' => ['housenum' => '30', 'buildnum' => '1', 'strucnum' => '2'],
      '30 корп. 1 с 2' => ['housenum' => '30', 'buildnum' => '1', 'strucnum' => '2'],

      '30 кк 1 строение 2' => ['housenum' => '30', 'buildnum' => '1', 'strucnum' => '2'],
      '30 кк 1 стр 2' => ['housenum' => '30', 'buildnum' => '1', 'strucnum' => '2'],
      '30 кк 1 с 2' => ['housenum' => '30', 'buildnum' => '1', 'strucnum' => '2'],

      '30 кк. 1 строение 2' => ['housenum' => '30', 'buildnum' => '1', 'strucnum' => '2'],
      '30 кк. 1 стр 2' => ['housenum' => '30', 'buildnum' => '1', 'strucnum' => '2'],
      '30 кк. 1 с 2' => ['housenum' => '30', 'buildnum' => '1', 'strucnum' => '2'],

      '30 к 1 строение 2' => ['housenum' => '30', 'buildnum' => '1', 'strucnum' => '2'],
      '30 к 1 стр 2' => ['housenum' => '30', 'buildnum' => '1', 'strucnum' => '2'],
      '30 к 1 с 2' => ['housenum' => '30', 'buildnum' => '1', 'strucnum' => '2'],

      '30 к. 1 строение. 2' => ['housenum' => '30', 'buildnum' => '1', 'strucnum' => '2'],
      '30 к. 1 стр. 2' => ['housenum' => '30', 'buildnum' => '1', 'strucnum' => '2'],
      '30 к. 1 с. 2' => ['housenum' => '30', 'buildnum' => '1', 'strucnum' => '2'],

      '30А корп 1 строение 2' => ['housenum' => '30А', 'buildnum' => '1', 'strucnum' => '2'],
      '30А корп 1 стр 2' => ['housenum' => '30А', 'buildnum' => '1', 'strucnum' => '2'],
      '30А корп 1 с 2' => ['housenum' => '30А', 'buildnum' => '1', 'strucnum' => '2'],

      '30А корп. 1 строение 2' => ['housenum' => '30А', 'buildnum' => '1', 'strucnum' => '2'],
      '30А корп. 1 стр 2' => ['housenum' => '30А', 'buildnum' => '1', 'strucnum' => '2'],
      '30А корп. 1 с 2' => ['housenum' => '30А', 'buildnum' => '1', 'strucnum' => '2'],

      '30а корп Б строение 2' => ['housenum' => '30А', 'buildnum' => 'Б', 'strucnum' => '2'],
      '30А корп б стр 2' => ['housenum' => '30А', 'buildnum' => 'Б', 'strucnum' => '2'],
      '30А корп Б с 2' => ['housenum' => '30А', 'buildnum' => 'Б', 'strucnum' => '2'],

      '30А корп. Б строение 2' => ['housenum' => '30А', 'buildnum' => 'Б', 'strucnum' => '2'],
      '30А корп. б стр 2' => ['housenum' => '30А', 'buildnum' => 'Б', 'strucnum' => '2'],
      '30а корп. Б с 2' => ['housenum' => '30А', 'buildnum' => 'Б', 'strucnum' => '2'],
    ];

    // Testing russian house numbers
    foreach ($expected as $input => $output) {
      $this->assertEquals($output, FiasAddressParser::parseHouse($input), "Fail on: $input");
    }
  }

  /**
   * Tests a static FiasAddressParser::parseStreet method.
   */
  public function testParseStreet() {
    $expected = [
      'ул. Василия Ивановича' => [
        'name' => 'Василия Ивановича',
        'type' => 'ул'
      ],
      'ул.Василия Ивановича' => [
        'name' => 'Василия Ивановича',
        'type' => 'ул'
      ],
      'ул Василия Ивановича' => [
        'name' => 'Василия Ивановича',
        'type' => 'ул'
      ],
      'улица Василия Ивановича' => [
        'name' => 'Василия Ивановича',
        'type' => 'ул'
      ],
      'бульвар Василия Ивановича Загорянского' => [
        'name' => 'Василия Ивановича Загорянского',
        'type' => 'б-р'
      ],
      '   проезд роковой ' => [
        'name' => 'Роковой',
        'type' => 'проезд'
      ],
      '  ал Радости ' => [
        'name' => 'Радости',
        'type' => 'аллея'
      ],
      'ш. Василия Ивановича' => [
        'name' => 'Василия Ивановича',
        'type' => 'ш'
      ],
      'ш.Василия Ивановича' => [
        'name' => 'Василия Ивановича',
        'type' => 'ш'
      ],
      'ш Василия Ивановича' => [
        'name' => 'Василия Ивановича',
        'type' => 'ш'
      ],
      'шоссе Василия Ивановича' => [
        'name' => 'Василия Ивановича',
        'type' => 'ш'
      ],
      'пр-т СЛАВЫ' => [
        'name' => 'Славы',
        'type' => 'пр-кт'
      ],
      'чонгарский бульв.' => [
        'name' => 'Чонгарский',
        'type' => 'б-р'
      ],
      // @ todo uncomment and fix this tests!
//      'перовская' => [
//        'name' => 'Перовская',
//        'type' => 'ул'
//      ],
//      'Алтайская' => [
//        'name' => 'Алтайская',
//        'type' => 'ул'
//      ],
    ];

    foreach ($expected as $input => $output) {
      $this->assertEquals($output, FiasAddressParser::parseStreet($input), "Fail on: $input");
    }
  }

}
